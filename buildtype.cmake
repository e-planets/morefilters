# Create a benchmark build type (max performance)
# Those flags are heavily compiler dependant
if(CMAKE_C_COMPILER_ID MATCHES "Clang")
  set(C_FLAGS_BENCHMARK "-Ofast -flto -DNDEBUG")
  set(LINKER_FLAGS_BENCHMARK "-flto")
  set(SHARED_LINKER_FLAGS_BENCHMARK "-flto")
elseif(CMAKE_C_COMPILER_ID MATCHES "GNU")
  set(C_FLAGS_BENCHMARK "-Ofast -flto -fprefetch-loop-arrays --param prefetch-latency=300 -DNDEBUG")
  set(EXE_LINKER_FLAGS_BENCHMARK "-flto")
  set(SHARED_LINKER_FLAGS_BENCHMARK "-flto")
elseif(CMAKE_C_COMPILER_ID MATCHES "Intel")
  set(C_FLAGS_BENCHMARK "-Ofast -fipo -qopt-prefetch -DNDEBUG")
  set(EXE_LINKER_FLAGS_BENCHMARK "-ipo")
  set(SHARED_LINKER_FLAGS_BENCHMARK "-ipo")
else()
  message(WARNING "Build type Benchmark not yet configured for ${CMAKE_C_COMPILER_ID}, using Release flags")
  set(C_FLAGS_BENCHMARK "${CMAKE_C_FLAGS_RELEASE}")
  set(EXE_LINKER_FLAGS_BENCHMARK "${CMAKE_C_FLAGS_RELEASE}")
  set(SHARED_LINKER_FLAGS_BENCHMARK "${CMAKE_C_FLAGS_RELEASE}")
endif()
set(CMAKE_C_FLAGS_BENCHMARK "${C_FLAGS_BENCHMARK}"
    CACHE STRING
    "Flags used by the C compiler during benchmark builds."
    FORCE)
set(CMAKE_EXE_LINKER_FLAGS_BENCHMARK "${EXE_LINKER_FLAGS_BENCHMARK}"
    CACHE STRING
    "Flags used for linking binaries during benchmark builds."
    FORCE)
set(CMAKE_SHARED_LINKER_FLAGS_BENCHMARK "${SHARED_LINKER_FLAGS_BENCHMARK}"
    CACHE STRING
    "Flags used by the shared libraries linker during benchmark builds."
    FORCE)
mark_as_advanced(
    CMAKE_C_FLAGS_BENCHMARK
    CMAKE_EXE_LINKER_FLAGS_BENCHMARK
    CMAKE_SHARED_LINKER_FLAGS_BENCHMARK)

if(NOT CMAKE_BUILD_TYPE) # Default build type
  set(CMAKE_BUILD_TYPE RelWithDebInfo
      CACHE STRING
      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel Benchmark."
      FORCE)
else() # Update description
  set(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE}
      CACHE STRING
      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel Benchmark."
      FORCE)
endif(NOT CMAKE_BUILD_TYPE)
