/* -*- Mode: C; tab-width: 4 -*- */
/*
 * Copyright CNRS
 *  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
 *
 * Various filters implemented for Python usage.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include <stdio.h>

#include "opencl_utils.h"

cl_device_id opencl_device = 0;
cl_context opencl_context = 0;
cl_command_queue opencl_queue = 0;

void
init_opencl(void)
{
	cl_int err = CL_SUCCESS;
	cl_platform_id *platforms, platform;
	cl_uint n_platforms = 0;
	cl_device_id *devs;
	cl_uint n_devs = 0;
#ifdef CL_VERSION_2_0
	const cl_queue_properties *queue_props = {0};
#endif

	if (getenv("MOREFILTERS_CL_DISABLE") != NULL)
		return;

	CLCHECK(clGetPlatformIDs(0, NULL, &n_platforms));
    platforms = alloca(n_platforms*sizeof(cl_platform_id));
	CLCHECK(clGetPlatformIDs(n_platforms, platforms, NULL));
	if (getenv("MOREFILTERS_CL_PLATFORM") == NULL) {
		platform = platforms[0];
	}
	else {
		char *s, *e;
		long num;
		s = getenv("MOREFILTERS_CL_PLATFORM");
		num = strtol(s, &e, 10);
		if (*s != '\0' && *e == '\0' && num >= 0 && num < n_platforms) {
			platform = platforms[num];
		}
		else {
			fprintf(	stderr, 
						"OpenCL error: MOREFILTERS_CL_PLATFORM number invalid\n");
			return;
		}
	}

	CLCHECK(clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &n_devs));
	devs = alloca(n_devs*sizeof(cl_device_id));
	CLCHECK(clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, n_devs, devs, NULL));
	if (getenv("MOREFILTERS_CL_DEVICE") == NULL) {
		opencl_device = devs[0];
	}
	else {
		char *s, *e;
		long num;
		s = getenv("MOREFILTERS_CL_DEVICE");
		num = strtol(s, &e, 10);
		if (*s != '\0' && *e == '\0' && num >= 0 && num < n_devs) {
			opencl_device = devs[num];
		}
		else {
			fprintf(	stderr, 
						"OpenCL error: MOREFILTERS_CL_DEVICE number invalid\n");
			return;
		}
	}

	opencl_context = clCreateContext(NULL, 1, &opencl_device, NULL, NULL, &err);
	CLCHECK(err);

    opencl_queue =
#ifdef CL_VERSION_2_0
		clCreateCommandQueueWithProperties(	opencl_context,
											opencl_device,
											queue_props,
											&err);
#else
		clCreateCommandQueue(	opencl_context,
								opencl_device,
								0,
								&err);
#endif
    CLCHECK(err);
}
