/* -*- Mode: C; tab-width: 4 -*- */
/*
 * Copyright CNRS
 *  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
 *
 * Various filters implemented for Python usage.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "macros.h"

inline void
hist(double *dist, const float *data, int xsize, int ysize)
{
	memset(dist, 0, 256*sizeof(double));
	for(int y = 0; y < ysize; y++) {
		for(int x = 0; x < xsize; x++) {
			float val = CLAMP(roundf(*(data+y*xsize+x)), 0.0, 255.0);
			if (isfinite(val))
				dist[(int)val] += 1.0;
		}
	}
}

void
histmatch(float *dst, const float *src, int src_xsize, int src_ysize, const float *ref, int ref_xsize, int ref_ysize)
{
	double src_dist[256];
	double ref_dist[256];
	float trans[256];

	#pragma omp parallel
    {
		#pragma omp single
		{
			#pragma omp task
			hist(src_dist, src, src_xsize, src_ysize);
			#pragma omp task
			hist(ref_dist, ref, ref_xsize, ref_ysize);
		}
    }
	for (int i = 1; i < 256; i++) {
		src_dist[i] += src_dist[i-1];
		ref_dist[i] += ref_dist[i-1];
	}
	for (int i = 0; i < 256; i++) {
		src_dist[i] /= src_dist[255];
		ref_dist[i] /= ref_dist[255];
	}

	memset(trans, 0, sizeof(trans));
	for (int i = 0; i < 256; i++) {
		int j1 = -1, j2 = 0;
		do j1++; while (j1 < 256 && ref_dist[j1+1] < src_dist[i]);
		j2 = j1;
		do j2++; while (j2 < 256 && ref_dist[j2] == ref_dist[j1]);

		trans[i] = j1 + (j2-j1)*((src_dist[i]-ref_dist[j1])/(ref_dist[j2]-ref_dist[j1]));
	}

	#pragma omp parallel for
	for(int y = 0; y < src_ysize; y++) {
		for(int x = 0; x < src_xsize; x++) {
			float val = CLAMP(roundf(*(src+y*src_xsize+x)), 0.0, 255.0);
			if (isfinite(val))
				dst[y*src_xsize+x] = trans[(int)val];
			else
				dst[y*src_xsize+x] = NAN;
		}
	}
}
