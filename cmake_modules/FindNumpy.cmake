if(Numpy_INCLUDE_DIRS)
  set(Numpy_FIND_QUIETLY TRUE)
endif(Numpy_INCLUDE_DIRS)

find_package(PythonInterp REQUIRED)

execute_process(
    COMMAND "${PYTHON_EXECUTABLE}" -c
            "from __future__ import print_function\ntry: import numpy; print(numpy.get_include(), end='')\nexcept:pass\n"
    OUTPUT_VARIABLE Numpy_INCLUDE_DIRS)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(
    Numpy DEFAULT_MSG
    Numpy_INCLUDE_DIRS)
MARK_AS_ADVANCED(Numpy_INCLUDE_DIRS)
