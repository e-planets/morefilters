# More filters...

A toolbox of 2D filters for Python/numpy. They are implemented in C and
aim to work with float32 data and should be extremely portable, and do not
require third party libraries.

## How to build

This project use CMake (<http://www.cmake.org>) to build and install its
components. We try to rely only on standard options, here are minimal
instructions to build morefilters using the default Makefile generator:

```shell
mkdir build
cd build
cmake ..
make
make install
```

A specific python can be targeted using the 
`-DPYTHON_EXECUTABLE=/full/path/to/python` cmake argument, for example
on MacOS, if you want to use MacPorts python version, use the following
cmake command:

```shell
cmake -DPYTHON_EXECUTABLE=/opt/local/python3 ..
```
