/* -*- Mode: C; tab-width: 4 -*- */
/*
 * Copyright CNRS
 *  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
 *
 * Various filters implemented for Python usage.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

extern void
bilateral(float *dst, const float *src, int xsize, int ysize, double sigma_d, double sigma_r);
extern void
histmatch(float *dst, const float *src, int src_xsize, int src_ysize, const float *ref, int ref_xsize, int ref_ysize);
extern void
median(float *dst, const float *src, int xsize, int ysize, int diameter);

#ifdef HAVE_OPENCL
# include "opencl_globals.h"
extern void
bilateral_opencl(float *dst, const float *src, int xsize, int ysize, double sigma_d, double sigma_r);
#endif

static PyObject *
_bilateral(PyObject *self, PyObject *args)
{
	PyObject *src_obj = NULL, *dst_obj = NULL;
	PyArrayObject *src_arr = NULL, *dst_arr = NULL;
	double sigma_d = 0., sigma_r = 0.;

	npy_intp *dims;
	const float *src_data;
	float *dst_data;

	(void)self;

	if (!PyArg_ParseTuple(args, "Odd", &src_obj, &sigma_d, &sigma_r)) {
		return NULL;
	}

	src_arr = (PyArrayObject *)PyArray_FROM_OTF(	src_obj,
													NPY_FLOAT32,
													NPY_ARRAY_IN_ARRAY);
	if (src_arr == NULL)
		goto error;
	dims = PyArray_DIMS(src_arr);
	dst_obj = PyArray_SimpleNew(2, dims, NPY_FLOAT32);
	if (dst_obj == NULL)
		goto error;
	dst_arr = (PyArrayObject *)PyArray_FROM_OTF(	dst_obj,
													NPY_FLOAT32,
													NPY_ARRAY_OUT_ARRAY);
	if (dst_arr == NULL)
		goto error;

	src_data = PyArray_DATA(src_arr);
	dst_data = PyArray_DATA(dst_arr);
	bilateral(dst_data, src_data, dims[1], dims[0], sigma_d, sigma_r);

	Py_DECREF(src_arr);
	Py_DECREF(dst_arr);
	Py_INCREF(dst_obj);
	return dst_obj;

error:
	Py_XDECREF(src_arr);
	return NULL;
}

#ifdef HAVE_OPENCL
static PyObject *
_bilateral_opencl(PyObject *self, PyObject *args)
{
	PyObject *src_obj = NULL, *dst_obj = NULL;
	PyArrayObject *src_arr = NULL, *dst_arr = NULL;
	double sigma_d = 0., sigma_r = 0.;

	npy_intp *dims;
	const float *src_data;
	float *dst_data;

	(void)self;

	if (!PyArg_ParseTuple(args, "Odd", &src_obj, &sigma_d, &sigma_r)) {
		return NULL;
	}

	src_arr = (PyArrayObject *)PyArray_FROM_OTF(	src_obj,
													NPY_FLOAT32,
													NPY_ARRAY_IN_ARRAY);
	if (src_arr == NULL)
		goto error;
	dims = PyArray_DIMS(src_arr);
	dst_obj = PyArray_SimpleNew(2, dims, NPY_FLOAT32);
	if (dst_obj == NULL)
		goto error;
	dst_arr = (PyArrayObject *)PyArray_FROM_OTF(	dst_obj,
													NPY_FLOAT32,
													NPY_ARRAY_OUT_ARRAY);
	if (dst_arr == NULL)
		goto error;

	src_data = PyArray_DATA(src_arr);
	dst_data = PyArray_DATA(dst_arr);
	bilateral_opencl(dst_data, src_data, dims[1], dims[0], sigma_d, sigma_r);

	Py_DECREF(src_arr);
	Py_DECREF(dst_arr);
	Py_INCREF(dst_obj);
	return dst_obj;

error:
	Py_XDECREF(src_arr);
	return NULL;
}
#endif

static PyObject *
_histmatch(PyObject *self, PyObject *args)
{
	PyObject *src_obj = NULL, *ref_obj = NULL, *dst_obj = NULL;
	PyArrayObject *src_arr = NULL, *ref_arr = NULL, *dst_arr = NULL;

	npy_intp *src_dims, *ref_dims;
	const float *src_data, *ref_data;
	float *dst_data;

	(void)self;

	if (!PyArg_ParseTuple(args, "OO", &src_obj, &ref_obj)) {
		return NULL;
	}

	src_arr = (PyArrayObject *)PyArray_FROM_OTF(	src_obj,
													NPY_FLOAT32,
													NPY_ARRAY_IN_ARRAY);
	if (src_arr == NULL)
		goto error;
	src_dims = PyArray_DIMS(src_arr);
	ref_arr = (PyArrayObject *)PyArray_FROM_OTF(	ref_obj,
													NPY_FLOAT32,
													NPY_ARRAY_IN_ARRAY);
	if (ref_arr == NULL)
		goto error;
	ref_dims = PyArray_DIMS(ref_arr);
	dst_obj = PyArray_SimpleNew(2, src_dims, NPY_FLOAT32);
	if (dst_obj == NULL)
		goto error;
	dst_arr = (PyArrayObject *)PyArray_FROM_OTF(	dst_obj,
													NPY_FLOAT32,
													NPY_ARRAY_OUT_ARRAY);
	if (dst_arr == NULL)
		goto error;

	src_data = PyArray_DATA(src_arr);
	ref_data = PyArray_DATA(ref_arr);
	dst_data = PyArray_DATA(dst_arr);
	histmatch(	dst_data,
				src_data, src_dims[1], src_dims[0],
				ref_data, ref_dims[1], ref_dims[0]);

	Py_DECREF(src_arr);
	Py_DECREF(ref_arr);
	Py_DECREF(dst_arr);
	Py_INCREF(dst_obj);
	return dst_obj;

error:
	Py_XDECREF(src_arr);
	Py_XDECREF(ref_arr);
	return NULL;
}

static PyObject *
_median(PyObject *self, PyObject *args)
{
	PyObject *src_obj = NULL, *dst_obj = NULL;
	PyArrayObject *src_arr = NULL, *dst_arr = NULL;
	int diameter;

	npy_intp *dims;
	const float *src_data;
	float *dst_data;

	(void)self;

	if (!PyArg_ParseTuple(args, "Oi", &src_obj, &diameter)) {
		return NULL;
	}

	src_arr = (PyArrayObject *)PyArray_FROM_OTF(	src_obj,
													NPY_FLOAT32,
													NPY_ARRAY_IN_ARRAY);
	if (src_arr == NULL)
		goto error;
	dims = PyArray_DIMS(src_arr);
	dst_obj = PyArray_SimpleNew(2, dims, NPY_FLOAT32);
	if (dst_obj == NULL)
		goto error;
	dst_arr = (PyArrayObject *)PyArray_FROM_OTF(	dst_obj,
													NPY_FLOAT32,
													NPY_ARRAY_OUT_ARRAY);
	if (dst_arr == NULL)
		goto error;

	src_data = PyArray_DATA(src_arr);
	dst_data = PyArray_DATA(dst_arr);
	median(dst_data, src_data, dims[1], dims[0], diameter);

	Py_DECREF(src_arr);
	Py_DECREF(dst_arr);
	Py_INCREF(dst_obj);
	return dst_obj;

error:
	Py_XDECREF(src_arr);
	return NULL;
}


static PyMethodDef morefilters_methods[] = {
	{"bilateral",
			_bilateral,
			METH_VARARGS,
			"Bilateral filter"},
#ifdef HAVE_OPENCL
	{"bilateral_opencl",
			_bilateral_opencl,
			METH_VARARGS,
			"Bilateral filter"},
#endif
	{"histmatch",
			_histmatch,
			METH_VARARGS,
			"Histogram matching"},
	{"median",
			_median,
			METH_VARARGS,
			"Median filter"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef moduledef = {
	PyModuleDef_HEAD_INIT,
	"morefilters",
	NULL,
	-1,
	morefilters_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

PyMODINIT_FUNC
PyInit_morefilters(void)
{
	PyObject *module;

	if ((module = PyModule_Create(&moduledef)) == NULL) {
		return NULL;
	}

	import_array();

#ifdef HAVE_OPENCL
	init_opencl();
#endif

	return module;
}
