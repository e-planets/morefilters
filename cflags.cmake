# Ensure we use C99
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")

# Add POSIX extensions over C99
add_definitions(-D_POSIX_C_SOURCE=200809L)
if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
  remove_definitions(-D_POSIX_C_SOURCE=200809L)
  add_definitions(-D__DARWIN_C_LEVEL=9000000)
endif()
if(${CMAKE_SYSTEM_NAME} MATCHES "FreeBSD")
  add_definitions(-D__BSD_VISIBLE=1)
endif()
if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
  add_definitions(-D_BSD_SOURCE=1 -D_DEFAULT_SOURCE=1)
endif()

# Compiler-specific enhanced build messages
if(CMAKE_C_COMPILER_ID MATCHES "Clang|GNU")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra")
endif()
if(CMAKE_C_COMPILER_ID MATCHES "Intel")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -w2")
endif()

# When using gcc, staticaly link libgcc to have more portable binaries
if(CMAKE_C_COMPILER_ID MATCHES "GNU")
  set(CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS} -static-libgcc")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -static-libgcc")
endif()
