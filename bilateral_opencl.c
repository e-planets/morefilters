/* -*- Mode: C; tab-width: 4 -*- */
/*
 * Copyright CNRS
 *  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
 *
 * Various filters implemented for Python usage.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "opencl_globals.h"

#define GAUSS2(x, y, s) (exp(-(x*x + y*y) / (2*s*s)))

const char *bilateral_clsources[1] = {"\
#define GAUSS(x, s) (exp(-(x*x)/(2*s*s)))\n\
\n\
kernel void\n\
b(global float *dst, global const float *src, int xsize, int ysize, int diameter, global float *kernel_d, float sigma_r)\n\
{\n\
	int x = get_global_id(0);\n\
	int y = get_global_id(1);\n\
	if (x >= xsize || y >= ysize) return;\n\
	kernel_d += (diameter*diameter)/2;\n\
	\n\
	const float src_center = *(src + xsize*y + x);\n\
	int halfdiameter = diameter/2;\n\
	float filtered = 0;\n\
	float wsum = 0;\n\
	\n\
	if (!isfinite(src_center)) {\n\
		*(dst + xsize*y + x) = NAN;\n\
		return;\n\
	}\n\
	\n\
	for (int j = -halfdiameter; j <= halfdiameter; j++) {\n\
		int y_offset = xsize * clamp(y+j, 0, ysize-1);\n\
		int j_offset = j * diameter;\n\
		for (int i = -halfdiameter; i <= halfdiameter; i++) {\n\
			int x_offset = clamp(x+i, 0, xsize-1);\n\
			const float src_neighbour = *(src + y_offset + x_offset);\n\
			float w;\n\
			if (!isfinite(src_neighbour))\n\
				continue;\n\
			w = kernel_d[j_offset + i] * GAUSS((src_neighbour - src_center), sigma_r);\n\
			filtered += (src_neighbour) * w;\n\
			wsum += w;\n\
		}\n\
	}\n\
	\n\
	*(dst + xsize*y + x) = filtered / wsum;\n\
}\n\
"};

void
bilateral_opencl(	float *dst,
			const float *src,
			int xsize, int ysize,
			double sigma_d, double sigma_r)
{
    int diameter = (int)(sigma_d*3.0)+(1-(int)(sigma_d*3.0)%2);
    int halfdiameter = diameter/2;
    float *kernel_d;

    kernel_d = alloca(diameter*diameter*sizeof(double));
    kernel_d += (diameter*diameter)/2;
    for (int y = -halfdiameter; y <= halfdiameter; y++) {
        for (int x = -halfdiameter; x <= halfdiameter; x++) {
            kernel_d[y*diameter + x] = GAUSS2(x, y, sigma_d);
        }
    }

	cl_int err = 0;
	cl_program program = 0;
	cl_kernel b = 0;
	cl_mem kernel_d_buf, src_buf, dst_buf;
	size_t local_work_size[3] = {16, 16, 1};
	size_t global_work_size[3] = {0, 0, 1};

	program = clCreateProgramWithSource(opencl_context, 1, bilateral_clsources, 0, &err);
	CLCHECK(err);
	err = clBuildProgram(program, 0, NULL, "-cl-fast-relaxed-math", NULL, NULL);
#ifndef NDEBUG
	if (err != CL_SUCCESS) {
		char build_log[20000];
		clGetProgramBuildInfo(
				program,
				opencl_device,
				CL_PROGRAM_BUILD_LOG,
				sizeof(build_log),
				build_log,
				NULL);
		printf("Build errors.\n%s\n", build_log);
	}
#endif
	CLCHECK(err);
	b = clCreateKernel(program, "b", &err);
	CLCHECK(err);

	kernel_d_buf = clCreateBuffer(	opencl_context,
									CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
									diameter*diameter*sizeof(float),
									kernel_d-(diameter*diameter)/2,
									&err);
	CLCHECK(err);
	src_buf = clCreateBuffer(	opencl_context,
								CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
								xsize*ysize*sizeof(float),
								(void *)src,
								&err);
	CLCHECK(err);
	dst_buf = clCreateBuffer(	opencl_context,
								CL_MEM_WRITE_ONLY,
								xsize*ysize*sizeof(float),
								NULL,
								&err);
	CLCHECK(err);
	
	global_work_size[0] = ((xsize-1)/local_work_size[0] + 1)*local_work_size[0];
	global_work_size[1] = ((ysize-1)/local_work_size[0] + 1)*local_work_size[1];
	clSetKernelArg(b, 0, sizeof(cl_mem), &dst_buf);
	clSetKernelArg(b, 1, sizeof(cl_mem), &src_buf);
	clSetKernelArg(b, 2, sizeof(int), &xsize);
	clSetKernelArg(b, 3, sizeof(int), &ysize);
	clSetKernelArg(b, 4, sizeof(int), &diameter);
	clSetKernelArg(b, 5, sizeof(cl_mem), &kernel_d_buf);
	float sigma_r2 = sigma_r;
	clSetKernelArg(b, 6, sizeof(float), &sigma_r2);
	err = clEnqueueNDRangeKernel(	opencl_queue, 
									b, 
									2,
									NULL, global_work_size, local_work_size, 
									0, NULL, NULL);
	CLCHECK(err);
	err = clEnqueueReadBuffer(	opencl_queue,
								dst_buf,
								CL_FALSE,
								0,
								xsize*ysize*sizeof(float),
								dst,
								0,
								NULL,
								NULL);
	CLCHECK(err);
	clFinish(opencl_queue);

	clReleaseMemObject(dst_buf);
	clReleaseMemObject(src_buf);
	clReleaseMemObject(kernel_d_buf);
	clReleaseKernel(b);
	clReleaseProgram(program);
}
