/* -*- Mode: C; tab-width: 4 -*- */
/*
 * Copyright CNRS
 *  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
 *
 * Various filters implemented for Python usage.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include <math.h>
#include <stdlib.h>

#include "macros.h"

#define SWAP(a,b) {register float temp=(a); (a)=(b); (b)=temp;}

static inline float
quickselect(float *arr, size_t n, size_t k) {
	size_t i, ir, j, l, mid;
	float a;

	l=0;
	ir=n-1;
	for(;;) {
		if (ir <= l+1) {
			if (ir == l+1 && arr[ir] < arr[l]) {
				SWAP(arr[l], arr[ir]);
			}
			return arr[k];
		}
		mid = (l+ir) >> 1;
		SWAP(arr[mid], arr[l+1]);
		if (arr[l] > arr[ir]) {
			SWAP(arr[l], arr[ir]);
		}
		if (arr[l+1] > arr[ir]) {
			SWAP(arr[l+1], arr[ir]);
		}
		if (arr[l] > arr[l+1]) {
			SWAP(arr[l], arr[l+1]);
		}
		i = l+1;
		j = ir;
		a = arr[l+1];
		for (;;) {
			do i++; while (arr[i] < a);
			do j--; while (arr[j] > a);
			if (j < i) break;
			SWAP(arr[i], arr[j]);
		}
		arr[l+1]=arr[j];
		arr[j]=a;
		if (j >= k) ir=j-1;
		if (j <= k) l=i;
	}
}

static inline void
m(float *dst, const float *src, int xsize, int ysize, int x, int y, int diameter)
{
	float *dst_center = dst + xsize*y + x;
	int halfdiameter = diameter/2;
	float *buf = NULL;
	int buflen = 0;

	buf = alloca(diameter*diameter*sizeof(float));
	for (int j = -halfdiameter; j < halfdiameter; j++) {
		int y_offset = xsize * CLAMP(y+j, 0, ysize-1);
		for (int i = -halfdiameter; i < halfdiameter; i++) {
			int x_offset = CLAMP(x+i, 0, xsize-1);
			const float *src_neighbour;
			src_neighbour = src + y_offset + x_offset;
			if (!isfinite(*src_neighbour))
				continue;
			buf[buflen] = *src_neighbour;
			buflen++;
		}
	}

	*dst_center = quickselect(buf, buflen, buflen/2);
}

void
median(float *dst, const float *src, int xsize, int ysize, int diameter)
{
	#pragma omp parallel for
	for(int y = 0; y < ysize; y++) {
		for(int x = 0; x < xsize; x++) {
			m(dst, src, xsize, ysize, x, y, diameter);
		}
	}
}
