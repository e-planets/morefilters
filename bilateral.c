/* -*- Mode: C; tab-width: 4 -*- */
/*
 * Copyright CNRS
 *  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
 *
 * Various filters implemented for Python usage.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include <math.h>
#include <stdlib.h>

#include "macros.h"

#ifdef WITH_EXP_EXP2_X_LOG2
# define LOG2 1.4426950408889634074
# define EXP(x) (exp2(x * LOG2))
#else
# define EXP(x) exp(x)
#endif
#define GAUSS(x, s) (EXP(-(x*x)/(2*s*s)))
#define GAUSS2(x, y, s) (EXP(-(x*x + y*y) / (2*s*s)))

static inline float
b(	const float *restrict src,
	int xsize, int ysize,
	int x, int y,
	int diameter, double *restrict kernel_d, double sigma_r)
{
	const double src_center = *(src + xsize*y + x);
	int halfdiameter = diameter/2;
	double filtered = 0;
	double wsum = 0;

	if (!isfinite(src_center)) {
		return NAN;
	}

	for (int j = -halfdiameter; j <= halfdiameter; j++) {
		int y_offset = xsize * CLAMP(y+j, 0, ysize-1);
		int j_offset = j * diameter;
		for (int i = -halfdiameter; i <= halfdiameter; i++) {
			int x_offset = CLAMP(x+i, 0, xsize-1);
			const double src_neighbour = *(src + y_offset + x_offset);
			double w;
			if (!isfinite(src_neighbour))
				continue;
			w = kernel_d[j_offset + i] * GAUSS((src_neighbour - src_center), sigma_r);
			filtered += (src_neighbour) * w;
			wsum += w;
		}
	}

	return filtered / wsum;
}

void
bilateral(	float *restrict dst,
			const float *restrict src,
			int xsize, int ysize,
			double sigma_d, double sigma_r)
{
	int diameter, halfdiameter;
	double *kernel_d;

	diameter = (int)(sigma_d*3.0);
	diameter += 1-diameter%2;
	halfdiameter = diameter/2;

	kernel_d = alloca(diameter*diameter*sizeof(double));
	kernel_d += (diameter*diameter)/2;
	for (int y = -halfdiameter; y <= halfdiameter; y++) {
		for (int x = -halfdiameter; x <= halfdiameter; x++) {
			kernel_d[y*diameter + x] = GAUSS2(x, y, sigma_d);
		}
	}

	#pragma omp parallel for
	for (int y = 0; y < ysize; y++) {
		for (int x = 0; x < xsize; x++) {
			dst[xsize*y +x] = b(src, xsize, ysize, x, y, diameter, kernel_d, sigma_r);
		}
	}
}
