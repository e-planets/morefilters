#!/usr/bin/env python3

import sys
import time
import numpy as np
import morefilters

data = np.load(sys.argv[1]).astype('float32')
t1 = time.time()
data2 =  morefilters.bilateral_opencl(data, 15.0, 70.0)
t2 = time.time()
if sys.stdout.isatty():
    data2 = (data2 - data2.min()) / (data2.max() - data2.min())
    for y in range(0, data2.shape[0], 32):
        for x in range(0, data2.shape[1], 16):
            c = int(data2[y, x]*24+232)
            sys.stdout.write('\033[48;5;%dm \033[m'%(c))
        sys.stdout.write('\n')
print(">> ", t2-t1, "s", sep='')
