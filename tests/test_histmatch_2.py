#!/usr/bin/env python3

import sys
import time
import numpy as np
import morefilters

ref = np.load(sys.argv[1]).astype('float32')
ref = ref[::2, ::2] # To have a reference with different dimensions
src = np.load(sys.argv[2]).astype('float32')
t1 = time.time()
out =  morefilters.histmatch(src, ref)
t2 = time.time()
print(">> ", t2-t1, "s", sep='')
